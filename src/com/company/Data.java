package com.company;

public class Data {
    private String filename;
    private String icon16;
    private String icon32;
    private String fileContent;

    public String getFilename() {
        return this.filename;
    }
    public String getIcon16() {
        return this.icon16;
    }
    public String getIcon32() {
        return this.icon32;
    }
    public String getFileContent() {
        return this.fileContent;
    }
    public void setFilename(String filename) {
        this.filename = filename;
    }
    public void setIcon16(String icon16) {
        this.icon16 = icon16;
    }
    public void setIcon32(String icon32) {
        this.icon32 = icon32;
    }
    public void setFileContent(String fileContent) {
        this.fileContent = fileContent;
    }
}
