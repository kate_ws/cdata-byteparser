package com.company;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParseService {

    private int byteLength;

    public Data parseFromString(String inputString, int byteLength) {
        this.byteLength = byteLength;
        Data data = new Data();
        try {
            String body = null;
            Pattern pattern = Pattern.compile("<!(.*?)<");
            Matcher matcher = pattern.matcher(inputString);
            if (matcher.find()) {
                body = matcher.group(1);
            }

            String filename = null;
            pattern = Pattern.compile("CDATA(.*?)>");
            matcher = pattern.matcher(body);
            if (matcher.find()) {
                filename = matcher.group(1).substring(1);
            }
            data.setFilename(filename);
            body = body.substring(filename.length() + 8);

            int icon16size = getSizeFromByteString(body, 4);
            body = body.substring(4 * byteLength);
            data.setIcon16(body.substring(0, icon16size * byteLength));
            body = body.substring(icon16size * byteLength);

            int icon32size = getSizeFromByteString(body, 4);
            body = body.substring(4 * byteLength);
            data.setIcon32(body.substring(0, icon32size * byteLength));
            body = body.substring(icon32size * byteLength);

            int fileSize = getSizeFromByteString(body, 4);
            body = body.substring(4 * fileSize);
            data.setFileContent(body.substring(0, fileSize * byteLength));
            body = body.substring(fileSize * byteLength);
        } finally {
            System.out.println("Data was corrupted");
        }
        return data;
    }

    private static byte[] getArray(List<Byte> byteList) {
        byte[] bytes = new byte[byteList.size()];
        for (int i = 0; i < byteList.size(); i++) {
            bytes[i] = byteList.get(i);
        }
        return bytes;
    }

    private int getSizeFromByteString(String body, int bytes) {
        String dataSize = body.substring(0, bytes * this.byteLength);
        String[] integerStrings = dataSize.split("#");
        List<Byte> byteList = new ArrayList<>();
        for (String integer : integerStrings) {
            if (!integer.equals("")) {
                byteList.add(Byte.valueOf(integer));
            }
        }
        Collections.reverse(byteList);
        return ByteBuffer.wrap(getArray(byteList)).getInt();
    }
}
